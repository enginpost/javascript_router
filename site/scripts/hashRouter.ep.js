( function( $ ){
  
  routeManager = new function(){
    //variables
    this.routeTable;

    this.setRouteTable = function( thisRouteTable ){
      for(var i = 0; i < thisRouteTable.routes.length; i++){
        var routeParts = thisRouteTable.routes[i].route.split('/');
        var regexRoute = '^\\#';
        for(var j = 0; j < routeParts.length; j++){
          regexRoute += ( routeParts[j].indexOf("{") > -1 ) ? "\\/[\\w{}'" + '"' +":_%\\-.,]+"  :  "\\/" + routeParts[j];
        }
        thisRouteTable.routes[i]['test'] = regexRoute + "$";
      }
      this.routeTable = thisRouteTable;
    }

    // methods
    this.watch_start = function(){
      var parent = this;
      $(window).on('hashchange', function(){
        if(window.location.hash != ''){
          RouteParts = window.location.hash.substring(2).split('/');
          for(var i = 0; i < parent.routeTable.routes.length; i++){
            var routeTest = new RegExp( parent.routeTable.routes[i].test );
            if( routeTest.test( window.location.hash ) ){
              parent.routeTable.routes[i].callback.call( this, getRouteData( parent.routeTable.routes[i].route.split('/'), RouteParts ) );
              break;
            }
          }
        }else{
          loadHome( parent.routeTable );
        }
      });
      if( window.location.hash != ''){
        $(window).trigger('hashchange');
      }else{
        loadHome( this.routeTable);
      }
    }
    this.watch_stop = function(){
       $(window).off('hashchange');
    }
  }

  function getRouteData( thisRouteDesign, thisRouteData ){
    var data = [];
    for(key in thisRouteDesign){
      if( thisRouteDesign[key].indexOf('{') > -1 ){
        data[ thisRouteDesign[key].replace(/[{}]/g,'') ] = thisRouteData[key];
      }
    }
    return data;
  }

  function loadHome( routeTable ){
    if( routeTable == undefined ){
      routeTable = this.routeTable;
    }
    for(var i = 0; i < routeTable.routes.length; i++){
      if( routeTable.routes[i].route == "#" ){
        routeTable.routes[i].callback.call( this );
      }
    }
  }

})( jQuery );