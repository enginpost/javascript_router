( function( $ ){
  $(window).ready(function(){

    //globally scoped variable
    favoriteColor = 'blue';
    //locally scoped varialbe
    var food = 'cake';

    //creating a local instance of a global class
    var routeMgr = Object.create( routeManager );

    //creating a local routeTable to define routes and parameters
    var routeTable = { routes : [
      { route : '#', callback : route_home},
      { route : '{userid}', callback : route_userid },
      { route : '{userid}/{drug}', callback : route_userid_drug },
      { route : '{userid}/{drug}/{strength}', callback : route_userid_drug_strength },
      { route : '{userid}/{drug}/{strength}/{form}', callback : route_userid_drug_strength_form },
      { route : '{userid}/{drug}/{strength}/{form}/{distance}', callback : route_userid_drug_strength_form_distance },
      { route : '{userid}/{drug}/{strength}/{form}/{distance}/loc/{location}', callback : route_userid_drug_strength_form_distance_location }
    ]};

    //creating functions to handle route match callbacks
    ////notice that the context of the callback is local. You can read the data as  well as see local and global variables and methods
    function route_home(){
      //sample path:  /# or no hash
      $('#route-title').html('This is home');
      $('#route-body').html('<p>This is the home page</p>');
    }
    function route_userid(data){
      //sample path:  #/steve.mcdonald
      $('#route-title').html('Function called: route_userid');
      $('#route-body').html('<p>data properties:' + showProps(data) + '</p><p>food: ' + food + '</p><p>favoriteColor: ' + favoriteColor + '</p>');
    }
    function route_userid_drug(data){
      //sample path:  #/steve.mcdonald/asprin
      $('#route-title').html('Function called: route_userid_drug');
      $('#route-body').html('<p>data properties:' + showProps(data) + '</p><p>food: ' + food + '</p><p>favoriteColor: ' + favoriteColor + '</p>');
    }
    function route_userid_drug_strength(data){
      //sample path:  #/steve.mcdonald/asprin/50mg
      $('#route-title').html('Function called: route_userid_drug_strength');
      $('#route-body').html('<p>data properties:' + showProps(data) + '</p><p>food: ' + food + '</p><p>favoriteColor: ' + favoriteColor + '</p>');
    }
    function route_userid_drug_strength_form(data){
      //sample path:  #/steve.mcdonald/asprin/50mg/tablet
      $('#route-title').html('Function called: route_userid_drug_strength_form');
      $('#route-body').html('<p>data properties:' + showProps(data) + '</p><p>food: ' + food + '</p><p>favoriteColor: ' + favoriteColor + '</p>');
    }
    function route_userid_drug_strength_form_distance(data){
      //sample path:  #/steve.mcdonald/asprin/50mg/tablet/5
      $('#route-title').html('Function called: route_userid_drug_strength_form_distance');
      $('#route-body').html('<p>data properties:' + showProps(data) + '</p><p>food: ' + food + '</p><p>favoriteColor: ' + favoriteColor + '</p>');
    }
    function route_userid_drug_strength_form_distance_location(data){
      // NOTE: the stringified object needs to use doublequotes around the properties and values. You cannot use litterals or singlequotes
       //sample path:  #/steve.mcdonald/asprin/50mg/tablet/5/loc/{"type":"geo","lat":"1.01","lng":"1.02"}
       //alt sample path:  #/steve.mcdonald/asprin/50mg/tablet/5/loc/{"type":"zip","zip":"54915"}
        //alt sample path:  #/steve.mcdonald/asprin/50mg/tablet/5/loc/{"type":"citystate","city":"Appleton","state":"WI"}
      $('#route-title').html('Function called: route_userid_drug_strength_form_distance_location');

      var bodyHtml = '<p>data properties:' + showProps(data) + '</p><p>food: ' + food + '</p><p>favoriteColor: ' + favoriteColor + '</p>'

      // exmaple of parsing the location parameter
      var thisLocation = JSON.parse( data.location );
      switch( thisLocation.type ){
        case 'geo':
          bodyHtml += '<p>We are geolocating. (lat: ' + thisLocation.lat + '; lng: ' +thisLocation.lng + ')</p>';
          break;
        case 'zip' :
          bodyHtml += '<p>We are using zipcode. (zip: ' + thisLocation.zip + ')</p>';
          break;
        default : //citystate
          bodyHtml += '<p>We are using city and state. (city: ' + thisLocation.city + '; state: ' +thisLocation.state + ')</p>';
          break;
      }
      $('#route-body').html( bodyHtml );
    }

    //local method to show the parameters coming back with route values as properties of the data object
    function showProps( theseProperties ){
      var message = '[ ';
      for( key in theseProperties ){
        message += key + ': ' + theseProperties[key] + '; ';
      }
      return message.substring(0, message.length-2) + ']';
    }


    //code actually starts here
    routeMgr.setRouteTable( routeTable );
    routeMgr.watch_start();

  } );
} )( jQuery );