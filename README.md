#JAVASCRIPT_ROUTER
##What it is made to do
The javascript_router project facilitates creating custom single-page-application (SPA) for entire websites or deep linking for interactivity within the single page of a website. The end effect is that it... 

* fixes the broken browser back button often experienced in ajaxian interactions on a website
* allows you to facilitate permalinks into interactive ajaxian experiences

##How it works
This is achieved by monitoring updates to the hash-portion of the URL and matching them to routes defined in your custom route table. The routeManager loads your custom routeTable which is a collection of routes, each one specifying a route signature

     {food_group}/{food_name}/{food_type}/calories

along with which javascript callback function to use 

     function api_food_calories(data)

The routeManager reads the custom routeTable and generates unique regular expressions (RegEx) for each route used to test updates to the URL hash.  This is how it identifies a route from the URL and calls the correct function to handle that route scenario.

When a URL's hash changes from 

     https://food.info/api_1"
to 

     https://food.info/api_1/#/fruit/orange/naval/calories  

the routeManager searches through the routeTable, testing each route's custom RegEx and calls it's matching function for handling the logic of that route.

##How to use the repo

###Setup the project using NODE.js

1. Go into the project folder
2. (Assuming Node is installed) Run the command: npm install
3. Run the command: gulp

###Editing the example

The the router uses jQuery to watch the on hashchange event. So that should be included in the *index.html* file in the **site** folder. Next take a look at the code sample in the *main.js* file in the **script** folder.

There are four steps to using the routeManager in main.js:

1. Create a local object instance of the routeManager.
2. Setup your routeTable with you route signatures and callback function references.
3. Write your callback functions locally and all but the home handler should be ready to receive a data object as an input to the function (more on the data object in a moment).
4. Tell the local object instance of the routeManager to start watching the URL.

###What's the Data object

When you create a route you specify which route URL parts should contain dynamic data by putting those URL parts in braces:

     {userid}/{mailbox}/messages/{messageid}

In the above route, you would expect the variables in braces to be replaced by real values:

     steve.mcdonald/inbox/messages/00125

Let's imagine that the above route was associated with the following callback

     getMailboxMessage

When you write that local function, you would expect the routeManager to hand you the variables defined in the route. Here is an example of how that local function might be written:

     function getMailboxMessage( data ){
          alert(  "User is " + data.userid + " and mailbox is " + data.mailbox + " and message requested is " + data.messageid );
     }

The routeManager looks at the hash of the real URL and takes the real values and puts them into same-named properties of the data object given to your route callback function. Your job is to define the route signature and put braces around the variable names, and the routeManager will get your callback function a data object with the information from the real URL hash.